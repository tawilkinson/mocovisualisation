# mocoVisualisation

A Julia script that can be used to reproduce motion logs.

## What you need
Julia, with the relevant packages installed [Makie]. You will also need a 3D model to display (you can also comment out that section if you like)

## Hardware setup
The computer running the script needs to be able to listen in on the prospective motion correction updates. You will also need an fMRI compatible monitor. I have tried to move the implementation specific information into the ks_configuration.jl file. If you'd like to add a new implementation I suggest adding a new file (like $(your_abbreviation)_configuration.jl)
![Hardware setup](media/figure1.png)

## When it works you should get something like this
![What it should look like](media/figure2.gif)